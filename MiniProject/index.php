<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title>Conception</title>
        <link rel="stylesheet" href="Resource/assets/css/bootstrap.min.css" />
<!--        <link rel="stylesheet" href="Resource/assets/font-awesome/css/font-awesome.min.css" />-->
        

        <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
        <link href="Resource/css/style2.css" rel="stylesheet">
	<link rel="stylesheet" href="Resource/css/style3.css">
        <link rel="stylesheet" href="Resource/assets/style.css" />
    </head>
    <body>
        
        <div class="wrapper">
			<header>
				<h1>Phone Book</h1>
                                <h2 style="color:#fff;" align="center">Group - Conception</h2>
                                
			</header>
				<nav>
					<ul>
						<li><a href="#">HOME</a></li>
						<li><a href="#">ABOUT US</a></li>
						<li><a href="#">ABOUT BASIS</a></li>
						<li><a href="#">CONTACT US</a></li>
					</ul>
				</nav>
				
			
    <section class="content">
        <div class="container">
            <div class="row">
                <h2 align="center">Login Control</h2>
                <div class="col-md-4 col-md-offset-4 login text-center">
                    
                    <h2>Login Panel</h2>
                    <hr />
                    <?php
                    include 'vendor/autoload.php';
                    session_start();
                    use App\Login;
                    use App\Requered;

                    $obj = new Login();
                    
                    if($_SESSION!=NULL){
                        $success = Requered::success_message();
                        $unsuccess = Requered::unsuccess_message();
                        if ($success) {
                            ?>
                    
                    
                        <div class = "alert alert-danger">
                            <button type = "button" class = "close" data-dismiss = "alert">
                                <i class = "fa fa-times"></i>
                            </button>
                               <p >
                                <strong>
                                    <i class = "ace-icon fa fa-times"></i>
                                    <!--                        Well done!-->
                                </strong>
                               <?php echo $success; ?>
                            </p>
                        </div>
                        <?php
                    }
                    }
                    ?>
                    <form action="check.php" method="post" class="form-horizontal">
                        <div class="form-group" id="important">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email Address</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="email_address" id="inputEmail3" placeholder="Please enter email address">
                            </div>
                        </div>
                        <div class="form-group" id="important">
                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Please enter password">
                            </div>
                        </div>
                        <div class="form-group" id="important">
                            <div class="row">
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px;">
                                <div class="col-sm-8 col-md-offset-2 log">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">Sign in</button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
                                
                                </section>
			
			<footer>
				<p>
                                    <b>Copyright &copy; Conception | Mini Project, BITM | All Rights Reserved</b>
				</p>
			</footer>
		</div>

           <script src="Resource/js/jqu.js" type="text/javascript"></script>
        <script type="text/javascript" src="Resource/assets/js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="Resource/assets/js/bootstrap.min.js"></script>
        <script>
            $('#message').hide(5000);
        </script>
    </body>
</html>