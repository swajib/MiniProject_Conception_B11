<?php

namespace App\BITM\PhoneBook\Utility;

class Utility{
    
    
    public $massage="";
    
    
    static public function d($param = false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param = false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/MiniProject/view/BASIS/PhoneBook/lists.php"){
        header("Location:".$url);
    }
    
    static public function massage($massage = null) {
        
        if(is_null($massage)){
            
           $_massage = $_SESSION['massage'];
            
            $_SESSION['massage'] = null;
            
            return  $_massage;
            
            
        }  else {
            
            $_SESSION['massage']=$massage;
            
//            var_dump($_SESSION);
//            die();
          
        }
        
        
    }
    
    
    
}

