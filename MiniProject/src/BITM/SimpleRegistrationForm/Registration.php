<?php

namespace App\BITM\SimpleRegistrationForm;
use App\Requered;
use App\BITM\Utility\Utility;

class Registration {

    public $id;
    public $fname;
    public $lname;
    public $birthday;
    public $email;
    public $cemail;
    public $phonenumber;
    public $city;
    public $religious;
    public $gender;
    public $aboutme;
    
    public $termandcondition;
    public $deleted_at;

    public function __construct($data = false) {

        if (is_array($data) && array_key_exists("id", $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->fname = $data['fname'];
        $this->lname = $data['lname'];
        $this->birthday = $data['birthday'];
        $this->email = $data['email'];
        $this->cemail = $data['cemail'];
        $this->phonenumber = $data['phonenumber'];
        $this->city = $data['city'];
        $this->religious = $data['religious'];
        $this->gender = $data['gender'];
        $this->aboutme = $data['aboutme'];
        $this->termandcondition = $data['termandcondition'];
    }

    public function lists() {

        $registrations = array();

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "SELECT * FROM `registrations` WHERE deleted_at IS NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {

            $registrations [] = $row;
        }
        return $registrations;
    }

    public function show($id = null) {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "SELECT * FROM `registrations` WHERE id=" . $id;
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;
    }

    public function delete($id = false) {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "DELETE FROM `miniproject`.`registrations` WHERE `registrations`.`id` =" . $id;

        $result = mysql_query($query);

//        var_dump($result);
//        die();

        if ($result) {

            Utility::massage("Data deletion succesfull");
        } else {

            Utility::massage("Cannot delete");
        }
        Utility::redirect("lists.php");
    }

    public function trash($id = false) {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

        //$this->id=  $id;
        $this->deleted_at = time();

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "UPDATE `miniproject`.`registrations` SET `deleted_at` = '" . $this->deleted_at . "' WHERE `registrations`.`id` =" . $id;

        $result = mysql_query($query);

//        var_dump($result);
//        die();

        if ($result) {

            Utility::massage("Data trashed succesfully");
        } else {

            Utility::massage("Cannot trash");
        }
        Utility::redirect("lists.php");
    }

    public function trashed() {

        $registrations = array();

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "SELECT * FROM `registrations` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {

            $registrations [] = $row;
        }
        return $registrations;
    }

    public function recover($id = false) {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "UPDATE `miniproject`.`registrations` SET `deleted_at` = NULL WHERE `registrations`.`id` =" . $id;

        $result = mysql_query($query);
        if ($result) {

            Utility::massage("Data recovered succesfully");
        } else {

            Utility::massage("Cannot recovered");
        }
        Utility::redirect("lists.php");
    }



    public function update() {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

//        $this->deleted_at = time();
        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "UPDATE `miniproject`.`registrations` SET `fname` = '" . $this->fname . "', `lname` = '" . $this->lname . "',"
                . " `birthday` = '" . $this->birthday . "', `email` = '" . $this->email . "', `cemail` = '" . $this->cemail . "', `phonenumber` = '" . $this->phonenumber . "',"
                . " `city` = '" . $this->city . "', `gender` = '" . $this->gender . "', `religious` = '" . $this->religious . "', `aboutme` = '" . $this->aboutme . " ' "
                ." WHERE `registrations`.`id` = " . $this->id;
        
//        var_dump($query);
//        die();
        $result = mysql_query($query);
        if ($result) {

            Utility::massage("Data is updated succesfully !!!");
        } else {

            Utility::massage("Sorry !! Cannot update");
        }
        Utility::redirect("lists.php");
    }

    public function store() {

        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "INSERT INTO `miniproject`.`registrations` ( `fname`, `lname`, `birthday`, `email`, `cemail`, `phonenumber`, `city`, `gender`, `religious`, `aboutme`, `termandcondition`) VALUES "
                . "('" . $this->fname . "','" . $this->lname . "','" . $this->birthday . "','" . $this->email . "','" . $this->cemail . "','" . $this->phonenumber . "','" . $this->city . "','" . $this->gender . "','" . $this->religious . "','" . $this->aboutme . "','" . $this->termandcondition . "')";

        $result = mysql_query($query);

//        var_dump($result);
//        die();

        if ($result) {

            Utility::massage("I am storing data succesfully");
        } else {

            Utility::massage("Data storing failed");
        }
        Utility::redirect("lists.php");
    }
    
    
    
    
       public function recoverall($ids = array()){
//        var_dump($ids);
         if(is_array($ids) && count($ids)>0){
            $_id = implode(", ", $ids);
        
    $query = "UPDATE `registrations` SET `deleted_at` = NULL WHERE `registrations`.`id` IN ($_id)";
   
            $result = mysql_query($query);
                        
        if ($result) {

            Utility::massage("Data recovered succesfully");
        } else {

            Utility::massage("Cannot recovered");
        }
        Utility::redirect("lists.php");
    }
    } 
    
    
      public function deleteall($ids = array()){
                
        if(is_array($ids) && count($ids)>0){
            $_id = implode(", ", $ids);
            
             
        $conn = mysql_connect("localhost", "root", "");
        $lnk = mysql_select_db("miniproject");

        $query = "DELETE FROM `miniproject`.`registrations` WHERE `registrations`.`id` IN($_id)";

        $result = mysql_query($query);

//        var_dump($result);
//        die();

        if ($result) {

            Utility::massage("Data deletion succesfull");
        } else {

            Utility::massage("Cannot delete");
        }
        Utility::redirect("lists.php");
    }
      }
    
    public function logout(){
        session_destroy();
        Requered::success_message("You are successfully logged out !!!");
        Requered::redirectout();
    }

}

