
<?php
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniProject' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\SimpleRegistrationForm\Registration;
use App\BITM\Utility\Utility;

$registration = new Registration($_POST);

$registrations = $registration->trashed();

//var_dump($registrations);
//die();
?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration form</title>
        <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../Resource/css/style2.css" rel="stylesheet">
	<link rel="stylesheet" href="../../../Resource/css/style3.css">

    </head>
    <body>
        <div class="wrapper">
			<header>
				<h1>Phone Book</h1>
                                <h2 style="color:#fff;" align="center">Group - Conception</h2>
                                
			</header>
                            <nav>
                                <ul>
                                     <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="#">ABOUT US</a></li>
                                    <li><a href="#">ABOUT BASIS</a></li>
                                    <li><a href="#">CONTACT US</a></li>
                                </ul>
                            </nav>
				
			
			<section class="content">

        <h2>Trashed lists</h2>
        <br/>
        <br/>
        
        <div>
            <form action="recoverall.php" method="POST">
                <a href="lists.php"><button type="button" title="Click here to show all list info" id="utility" class="btn btn-info">List Information</button></a>
                <button class="btn btn-success" type="submit">RecoverAll</button>
                <button class="btn btn-danger" type="button" id="deleteAll">DeleteAll</button>
        </div>
                <br>
            <table border='2' class="table table-bordered table-hover table-striped">
                <thead>
<!--            <div><a href="create.php">Add new</a> | <span id="utility">Download as PDF | XLs </span></div>-->
                    <tr style="background: #16A085; color: #fff;">
                        <th><input type="checkbox" name="markall" id="markall" ></th>
                        <th>Sl.<i class="fa fa-arrow-down"></i></th>
                        
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Birthday</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                        <th>City</th>
                        <th>Gender</th>
                        <th>Religious</th>
                        <th>About Me</th>
                        
                        <th>Action</th>

                    </tr>
                </thead>
                <tbody>
                <?php
                if (count($registrations) > 0) {

                    $sino = 1;
                    foreach ($registrations as $registration) {
                        ?>
                            <tr>
                                <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $registration->id; ?>"></td>
                                <td><?php echo $sino; ?></td>
                                <td><?php echo $registration->fname; ?></td>
                                <td><?php echo $registration->lname; ?></td>
                                <td><?php echo $registration->birthday; ?></td>
                                <td><?php echo $registration->email; ?></td>
                                <td><?php echo $registration->phonenumber; ?></td>
                                <td><?php echo $registration->city; ?></td>
                                <td><?php echo $registration->gender; ?></td>
                                <td><?php echo $registration->religious; ?></td>
                                <td><?php echo $registration->aboutme; ?></td>
                                <td> 
                                       <a class="recover" href="recover.php?id=<?php echo $registration->id; ?>"><input class="btn btn-success btn-sm" type="button" value="Recovery"/></a> 
                                    <a class="delete" href="delete.php?id=<?php echo $registration->id; ?>"><input class="btn btn-danger btn-sm" type="button" value="Delete"/></a> 
                                </td>

                            </tr>

                            <?php
                            $sino++;
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="14">No record is available.</td>
                        </tr> 
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            </form>
        <hr/>
        <nav style="float: right; background: #EEEEEE; padding-right: 470px">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true"><<</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">>></span>
                    </a>
                </li>
            </ul>



            <!--        java script start here....-->
            <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
            <script src="../../../Resource/js/jqu.js" type="text/javascript"></script>
            <script>
                $(document).ready(function () {
                    $(".delete").bind('click', function (e) {

                        var deleteitem = confirm('Are you sure you want to delete it?');

                        if (!deleteitem) {

                            e.preventDefault();
                        }


                    });

                    $('#message').hide(5000);

                    $('#markall').bind('click', function () {
                        if ($('#markall').is(':checked')) {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "mark"               
                            });
                        } else {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = false;  //select all checkboxes with class "mark"               
                            });
                        }
                    });

                    $('#deleteAll').bind('click', function (e) {

                        var startDeleteProcess = false;
                        $('.mark').each(function () { //loop through each checkbox
                            if ($(this).is(':checked')) {
                                startDeleteProcess = true;
                            }
                        });

                        if (startDeleteProcess) {
                            var deleteItem = confirm("Are you sure you want to delete all Items??");
                            if (deleteItem) {
                                document.forms[0].action = 'deleteall.php';
                                document.forms[0].submit();
                            }
                        } else {
                            alert("Please select an item first");
                        }


                    });


                });

            </script>
            
            </section>
			
			<footer>
				<p>
					 <b>Copyright &copy; Conception | Mini Project, BITM | All Rights Reserved</b>
				</p>
			</footer>
		</div>

    </body>
</html>
