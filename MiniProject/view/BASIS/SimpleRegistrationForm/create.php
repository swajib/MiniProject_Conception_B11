<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Conception</title>
        <link href='https://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
        <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../Resource/css/style2.css" rel="stylesheet">
        <link rel="stylesheet" href="../../../Resource/css/style3.css">
        <style>
        #utility{
            color: red;
        }
        </style>
    </head>
<body>
		
    <div class="wrapper">
        <header>
            <h1>Imagination is better than knowledge</h1>
        </header>
            <nav>
                <ul>
                    <li><a href="../../../index.php">HOME</a></li>
                    <li><a href="#">ABOUT US</a></li>
                    <li><a href="#">ABOUT BASIS</a></li>
                    <li><a href="#">CONTACT ME</a></li>
                </ul>
            </nav>
        <section class="content">
            <div class="container">
                <div class="row">
                    <div style="height: 1200px">
                        <div class="col-sm-3 col-md-3"></div>
                        <div style="padding-top: 60px; width:350px;" class="col-sm-5 col-md-5">    
                            <div class="centered centered">
                                <form action="store.php" method="post" id="registration-form" class="form-horizontal">
                                    <h2 align="center">Registration Form</h2>
                                    <div style="margin-right:-200px;">
                                        <a href="logout.php"><button style="margin-right:-82px;" type="button" title="Click here to logout your session" class="btn btn-danger pull-right button">Logout</button></a>
                                    </div>
                                    
                                    <div style="margin-right:-200px;">
                                        <a href="lists.php"><button type="button" title="Click here to show all list info" class="btn btn-info pull-right button">Registered List</button></a>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="fname" >First Name<span id='utility'>*</span></label>
                                        <input id="fname" class="form-control" type="text" tabindex="1" name="fname" placeholder="Please write your first name" required="required" autofocus="autofocus"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="lname">Last Name<span id='utility'>*</span></label>
                                        <input id="lname" class="form-control" type="text" tabindex="2" name="lname" placeholder="Please write your last name" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="birthday">Date of Birth<span id='utility'>*</span></label>
                                        <input id="birthday" class="form-control" type="date" tabindex="3" name="birthday" placeholder="YYYY-MM-DD" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="email">Email Address<span id='utility'>*</span></label>
                                        <input id="email" class="form-control" type="email" tabindex="4" name="email" placeholder="Please write your email address" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="cemail">Confirm Email Address<span id='utility'>*</span></label>
                                        <input id="cemail" class="form-control" type="email" tabindex="5" name="cemail" placeholder="Please write confirm email address" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="phonenumber">Cell Number<span id='utility'>*</span></label>
                                        <input id="phonenumber" class="form-control" type="text" tabindex="6" name="phonenumber" placeholder="Please write valid cell number" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="city">City<span id='utility'>*</span></label>
                                        <select id="city" tabindex="7" required="required" name="city" class="form-control">

                                            <option>__Select One__</option>
                                            <option>Dhaka</option>
                                            <option>Chittagong</option>
                                            <option>Rajshahi</option>
                                            <option>Sylhet</option>
                                            <option>Borisal</option>
                                            <option>Khulna</option>
                                            <option>Mymansing</option>

                                        </select>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="gen">Gender<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="gen" type="radio" tabindex="8" name="gender" value="Male" /> Male
                                        <br/>
                                        <input id="gen" type="radio" tabindex="9" name="gender" value="Female" /> Female
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="rel">Your Religious<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="10" name="religious" value="Islam"  /> Islam
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="11" name="religious" value="Christianity" /> Christianity
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="12" name="religious" value="Hinduism" /> Hinduism
                                    </div>
                                    <br/>
                                    <div>
                                        <label>About Me<span id='utility'>*</span></label>
                                        <br/>
                                        <textarea name="aboutme" class="form-control" rows="3" tabindex="13">
                                        </textarea>

                                    </div>
                                    <br/>
                                    <div>
                                        <input type="checkbox" tabindex="14" name="termandcondition" value="agree" required="required" />
                                        <label>I agree with this terms and condition.<span id='utility'>*</span></label>
                                    </div>
                                    <br/>
                                    <br/>
                                    <input id="register-form" class="btn btn-success" type="submit" tabindex="15" name="submit" value="Save" /> <input class="btn btn-danger" type="reset" tabindex="16" name="reset" value="Reset" />
                                    <br/>
                                    <hr/>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
				

<script src="../../../Resource/assets/js/jquery-1.7.1.min.js"></script> 

<script src="../../../Resource/assets/js/jquery.validate.js"></script> 

<script src="../../../script2.js"></script> 
<script>
		addEventListener('load', prettyPrint, false);
		$(document).ready(function(){
		$('pre').addClass('prettyprint linenums');
			});
		</script> 
				
			</section>
			
			<footer>
				<p>
					 <b>Copyright &copy; Conception | BITM | All Rights Reserved</b>
				</p>
			</footer>
		</div>
		
	</body>
</html>