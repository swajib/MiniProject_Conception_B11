
<?php
//var_dump($_POST);
//die();
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'MiniProject'.DIRECTORY_SEPARATOR.'view'.DIRECTORY_SEPARATOR.'startup.php');

use App\BITM\SimpleRegistrationForm\Registration;
use App\BITM\Utility\Utility;


$registration = new Registration();

$registrations = $registration->show($_REQUEST['id']);
?>



<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Conception</title>
        <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../Resource/css/style2.css" rel="stylesheet">
	<link rel="stylesheet" href="../../../Resource/css/style3.css">
    </head>
    <body>
        
        <div class="wrapper">
			<header>
				<h1>Phone Book</h1>
                                <h2 style="color:#fff;" align="center">Group - Conception</h2>
                                
			</header>
				<nav>
					<ul>
						<li><a href="#">HOME</a></li>
						<li><a href="#">ABOUT US</a></li>
						<li><a href="#">ABOUT BASIS</a></li>
						<li><a href="#">CONTACT US</a></li>
					</ul>
				</nav>
				
			
			<section class="content">
               
                        <div style="padding-top: 40px; margin-left: 340px; font-size: 16px;" class="col-sm-5 col-md-5">    
                            <div class="centered centered">
                                <form class="form-horizontal">
                                    <label><h2>Registered User Information</h2></label>
                                    <div>
                                        <a href="lists.php"><button type="button" title="Click here to show list information" class="btn btn-primary pull-right button" data-toggle="modal" data-target=".bs-example-modal-lg" ><strong><i class="fa fa-plus"></i>Registered List</strong> </button></a> 
                                    </div>
                                    <br/>
                                    <br/>
                                    <div><b>ID No. : </b><?php echo $registrations->id ?> </div>
                                    <br/>
                                    <div><b>First Name : </b><?php echo $registrations->fname ?> </div>
                                    <br/>
                                    <div><b>Last Name : </b><?php echo $registrations->lname ?> </div>
                                    <br/>
                                    <div><b>Date of Birth : </b><?php echo $registrations->birthday ?> </div>
                                    <br/>
                                    <div><b>Email address : </b><?php echo $registrations->email ?> </div>
                                    <br/>
                                    <div><b>Confirm Email Address : </b><?php echo $registrations->cemail ?> </div>
                                    <br/>
                                    <div><b>Cell Number : </b><?php echo $registrations->phonenumber ?> </div>
                                    <br/>
                                    <div><b>City : </b><?php echo $registrations->city ?> </div>
                                    <br/>
                                    <div><b>Gender : </b><?php echo $registrations->gender ?> </div>
                                    <br/>
                                    <div><b>Religious : </b><?php echo $registrations->religious ?> </div>
                                    <br/>
                                    <div><b>About Me : </b><?php echo $registrations->aboutme ?> </div>


                                    <br>
                                    <hr/>
                                    
                                </form>

                            </div>
                        </div>
    </section>
			
            <footer>
                <p>
                    <b>Copyright &copy; Conception | Mini Project, BITM | All Rights Reserved</b>
                </p>
            </footer>
    </div>

    </body>
</html>
