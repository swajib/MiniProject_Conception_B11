<?php
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniProject' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\SimpleRegistrationForm\Registration;
use App\BITM\Utility\Utility;

$registration = new Registration();

$registrations = $registration->lists();

//var_dump($registrations);
//die();

?>

<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Conception</title>
		<link href='https://fonts.googleapis.com/css?family=Oxygen:400,700' rel='stylesheet' type='text/css'>
                <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
                <link href="../../../Resource/css/style2.css" rel="stylesheet">
		<link rel="stylesheet" href="../../../Resource/css/style3.css">
                
        <style>

            #search{

                float: right;

            }

        </style>
                
	</head>
	<body>
		
		<div class="wrapper">
			<header>
				<h1>Phone Book</h1>
                                <h2 style="color:#fff;" align="center">Group - Conception</h2>
                                
			</header>
                        <nav>
                            <ul>
                                 <li><a href="../../../index.php">HOME</a></li>
                                <li><a href="#">ABOUT US</a></li>
                                <li><a href="#">ABOUT BASIS</a></li>
                                <li><a href="#">CONTACT US</a></li>
                            </ul>
                        </nav>
				
			
			<section class="content">
                            
                           
                                <h2 align="center">Registered List Information</h2>
        <br/>
        
             <?php
            include '../vendor/autoload.php';

         
            use App\Requered;



            $success = Requered::success_message();
            $unsuccess = Requered::unsuccess_message();
            if ($success) {
                ?>
                <div class = "alert alert-success">
                    <button type = "button" class = "close" data-dismiss = "alert">
                        <i class = " fa fa-times"></i>
                    </button>
                    <p>
                        <strong>
                            <i class = "ace-icon fa fa-check"></i>
                            <!-- Well done! Swajib-->
                        </strong>
                        <?php echo $success; ?>
                    </p>
                </div>
                <?php
            }if ($unsuccess) {
                ?>
                <div class = "alert alert-danger">
                    <button type = "button" class = "close" data-dismiss = "alert">
                        <i class = " fa fa-times"></i>
                    </button>
                    <p>
                        <strong>
                            <i class = "ace-icon fa fa-check"></i>
                            Sorry!
                        </strong>
                        <?php echo $unsuccess; ?>
                    </p>
                </div>
                <?php
            }
            ?>
        <br/>
        
            <div class = "alert alert-success">
                    <button type = "button" class = "close" data-dismiss = "alert">
                        <i class = "fa fa-times"></i>
                    </button>
                    <p>
                        <strong>
                            <i class = "ace-icon fa fa-check"></i>
                            <!-- Well done! Swajib-->
                        </strong>
                        <?php echo Utility::massage(); ?>
                    </p>
                </div>
        <br>
        
        <div>
                  <a href="logout.php"><button style="" type="button" title="Click here to logout your session" class="btn btn-danger pull-right button">Logout</button></a>
          </div>

        <div>
            <a href="create.php"><button style="margin-right:10px;" type="button" title="Click here to add registration" class="btn btn-primary pull-right button" data-toggle="modal" data-target=".bs-example-modal-lg" ><strong><i class="fa fa-plus"></i>Add Registration</strong> </button></a> 
            <a href="pdf.php"><button type="button" title="Click here to download list as PDF" id="utility" class="btn btn-success">Download as PDF</button></a> 
            <a href="01simple-download-xlsx.php"><button type="button" title="Click here to download list as XL" id="utility" class="btn btn-info">Download as XL</button></a> 
            <a href="trashed.php"><button type="button" title="Click here to show all trashed data" id="utility" class="btn btn-warning">All Trashed Data</button></a>
     
        </div>
        <br>
        
        <table border='2' class="table table-bordered table-hover table-striped"> 
            <tr style="background: #16A085; color: #fff;">

                <th>Sl.<i class="fa fa-arrow-down"></i></th>
                
                <th>First Name</th>
                <th>Last Name</th>
                <th>Date of Birth</th>
                <th>Email Address</th>
                <th>Cell Number</th>
                <th>City</th>
                <th>Gender</th>
                <th>Religion</th>
                <th>About Me</th>
                
                <th>Action</th>

            </tr>

            <?php
            $sino = 1;
            foreach ($registrations as $registration) {
                ?>
                <tr>    
                    <td><?php echo $sino; ?></td>
                    
                    <td><?php echo $registration->fname; ?></td>
                    <td><?php echo $registration->lname; ?></td>
                    <td><?php echo $registration->birthday; ?></td>
                    <td><?php echo $registration->email; ?></td> 
                    <td><?php echo $registration->phonenumber; ?></td>
                    <td><?php echo $registration->city; ?></td>
                    <td><?php echo $registration->gender; ?></td>
                    <td><?php echo $registration->religious; ?></td>
                    <td><?php echo $registration->aboutme; ?></td>
                    
                    <td>
                        <a href="show.php?id=<?php echo $registration->id; ?>"><input class="btn btn-primary btn-sm" type="submit" value="View"/></a> 
                        <a class="edit" href="edit.php?id=<?php echo $registration->id; ?>"><input class="btn btn-info btn-sm" type="submit" value="Edit"/></a> 
                        <a class="delete" href="delete.php?id=<?php echo $registration->id; ?>"><input class="btn btn-danger btn-sm" type="submit" value="Delete"/></a> 
                        <a class="trash" href="trash.php?id=<?php echo $registration->id; ?>"><input class="btn btn-warning btn-sm" type="submit" value="Trash"/></a>
                    </td>

                </tr>

                <?php
                $sino++;
            }
            ?>

        </table>
        
        <nav style="float: right; background: #EEEEEE; padding-right: 470px">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true"><<</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">>></span>
                    </a>
                </li>
            </ul>

        </nav>
            <script src="https://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript"></script>

            <script>

                $(".delete").bind('click', function (e) {

                    var deleteitem = confirm('Are you sure you want to delete this row?');

                    if (!deleteitem) {

                        e.preventDefault();
                    }


                })


            </script>
                            
				

<script src="../../../Resource/assets/js/jquery-1.7.1.min.js"></script> 

<script src="../../../Resource/assets/js/jquery.validate.js"></script> 

<script src="../../../script2.js"></script> 
<script>
		addEventListener('load', prettyPrint, false);
		$(document).ready(function(){
		$('pre').addClass('prettyprint linenums');
			});
		</script> 
				
			</section>
			
			<footer>
				<p>
					 <b>Copyright &copy; Conception | BITM | All Rights Reserved</b>
				</p>
			</footer>
		</div>
		
	</body>
</html>