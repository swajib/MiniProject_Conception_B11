
<?php
//var_dump($_POST);
//die();
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniProject' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\SimpleRegistrationForm\Registration;
use App\BITM\Utility\Utility;

$registration = new Registration();

$registrations = $registration->show($_REQUEST['id']);

//print_r($registrations);
//exit();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Conception</title>
        <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
        <link href="../../../Resource/css/style2.css" rel="stylesheet">
		<link rel="stylesheet" href="../../../Resource/css/style3.css">

        <style>

            #utility{

                color: red;

            }

        </style>

    </head>
    <body>
        
        <div class="wrapper">
			<header>
				<h1>Imagination is better than knowledge</h1>
			</header>
                            <nav>
                                <ul>
                                    <li><a href="../../../index.php">HOME</a></li>
                                    <li><a href="#">ABOUT US</a></li>
                                    <li><a href="#">ABOUT BASIS</a></li>
                                    <li><a href="#">CONTACT US</a></li>
                                </ul>
                            </nav>
				
			
			<section class="content">
                            <div style="margin-right:200px;">
                                <a href="lists.php"><button type="button" style="margin-top:110px;" title="Click here to show all list info" class="btn btn-info pull-right button">Registered List</button></a>
                                    </div>
                        <div style="padding-top: 50px; margin-left: 310px;" class="col-sm-5 col-md-5">    
                            <div class="centered centered">

                                <form action="update.php" method="POST" class="form-horizontal">
                                    <h2>Registration Information<br><small>(You can edit your registration information)</small></h2>
                                    <br/>
                                    <input type="hidden" name="id" value="<?php echo $registrations->id ?>"/>
                                    <div>
                                        <label for="fname">First Name<span id='utility'>*</span></label>
                                        <input id="fname" class="form-control" type="text" tabindex="1" name="fname" value="<?php echo $registrations->fname; ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="lname">Last Name<span id='utility'>*</span></label>
                                        <input id="lname" class="form-control" type="text" tabindex="2" name="lname" value="<?php echo $registrations->lname; ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="birthday">Date of Birth<span id='utility'>*</span></label>
                                        <input id="birthday" class="form-control" type="date" tabindex="3" name="birthday" value="<?php echo $registrations->birthday; ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="email">Email Address<span id='utility'>*</span></label>
                                        <input id="email" class="form-control" type="email" tabindex="4" name="email"  
                                               value="<?php echo $registrations->email ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="cemail">Confirm Email Address<span id='utility'>*</span></label>
                                        <input id="cemail" class="form-control" type="email" tabindex="5" name="cemail"
                                               value="<?php echo $registrations->cemail ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="phonenumber">Cell Number<span id='utility'>*</span></label>
                                        <input id="phonenumber" class="form-control" type="number" tabindex="6" name="phonenumber" 
                                               value="<?php echo $registrations->phonenumber; ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="city">City<span id='utility'>*</span></label>
                                        <select id="city" class="form-control" tabindex="7" required="required" name="city" >


                                            <?php if ($registrations->city == "Dhaka") { ?>
                                                <option value="<?php echo $registrations->city ?>" >Dhaka</option>
                                                <option>Chitagong</option>
                                                <option>Rajshahi</option>
                                                <option>Shylet</option>
                                                <option>Borisal</option>
                                                <option>Khulna</option>
                                                <option>Moymonshing</option>
                                            <?php } elseif ($registrations->city == "Chittagong") { ?>
                                                <option value="<?php echo $registrations->city ?>">Chittagong</option>

                                            <?php } elseif ($registrations->city == "Rajshahi") { ?>
                                                <option value="<?php echo $registrations->city ?>">Rajshahi</option>

                                            <?php } elseif ($registrations->city == "Sylhet") { ?>
                                                <option value="<?php echo $registrations->city ?>">Sylhet</option>

                                            <?php } elseif ($registrations->city == "Borisal") { ?>
                                                <option value="<?php echo $registrations->city ?>">Borisal</option>

                                            <?php } elseif ($registrations->city == "Khulna") { ?>
                                                <option value="<?php echo $registrations->city ?>">Khulna</option>

                                            <?php } elseif ($registrations->city == "Mymensing") { ?>
                                                <option value="<?php echo $registrations->city ?>">Mymensing</option>
                                                <option>Dhaka</option>
                                                <option>Chittagong</option>
                                                <option>Rajshahi</option>
                                                <option>Sylhet</option>
                                                <option>Borisal</option>
                                                <option>Khulna</option>
                                                <option>Mymensing</option>

                                            <?php } else { ?>

                                                <option>__Select One__</option>
                                                <option>Dhaka</option>
                                                <option>Chittagong</option>
                                                <option>Rajshahi</option>
                                                <option>Sylhet</option>
                                                <option>Borisal</option>
                                                <option>Khulna</option>
                                                <option>Mymensing</option>

                                            <?php } ?>

                                        </select>
                                    </div>
                                    <br/>
                                    <?php if ($registrations->gender == "Male") { ?>
                                        <div>

                                            <label for="gen">Gender<span id='utility'>*</span></label>
                                            <br/>
                                            <input id="gen" type="radio" tabindex="8" checked="" name="gender" required="required" value="<?php echo $registrations->gender; ?>" /> Male
                                            <br/><input id="gen" type="radio" tabindex="9" name="gender" required="required" value="<?php echo $registrations->gender ?>" /> Female

                                        </div>
                                    <?php } else { ?>
                                        <div>
                                            <input id="gen" type="radio"  name="gender" required="required" value="<?php echo $registrations->gender ?>" /> Male<br/>
                                            <input id="gen" type="radio" tabindex="9" name="gender" checked="" required="required" value="<?php echo $registrations->gender ?>" /> Female

                                        </div>
                                    <?php } ?>
                                    <br/>

                                    <div>
                                        <label for="rel" >Religious<span id='utility'>*</span></label>
                                        <?php if ($registrations->religious == "Islam") { ?>
                                            <br/>
                                            <br/><input id="rel" type="checkbox" checked="" tabindex="10" name="religious"  value="<?php echo $registrations->religious ?>"  />Islam
                                            <br/><input id="rel" type="checkbox" tabindex="12" name="religious"  value="<?php echo $registrations->religious ?>" />Hinduism
                                            <br/><input id="rel" type="checkbox" tabindex="11" name="religious"  value="<?php echo $registrations->religious ?>" />Christianity
                                            <br/>
                                        <?php } elseif ($registrations->religious == "Christianity") { ?>
                                            <br/><input id="rel" type="checkbox" tabindex="10" name="religious"  value="<?php echo $registrations->religious ?>"  />Islam
                                            <br/><input id="rel" type="checkbox" tabindex="12" name="religious"  value="<?php echo $registrations->religious ?>" />Hinduism
                                            <br/><input id="rel" type="checkbox" checked="" tabindex="11" name="religious"  value="<?php echo $registrations->religious ?>" />Christianity
                                        <?php } elseif ($registrations->religious == "Hinduism") { ?>
                                            <br/>
                                            <br/><input id="rel" type="checkbox" tabindex="10" name="religious"  value="<?php echo $registrations->religious ?>"  />Islam
                                            <br/><input id="rel" type="checkbox" checked="" tabindex="12" name="religious"  value="<?php echo $registrations->religious ?>" />Hinduism
                                            <br/><input id="rel" type="checkbox" tabindex="11" name="religious"  value="<?php echo $registrations->religious ?>" />Christianity
                                        <?php } ?>
                                    </div>
                                    <br/>
                                    <div>
                                        <label>About Me<span id='utility'>*</span></label>
                                        <br/>
                                        <textarea name="aboutme" class="form-control" rows="3" >
                                            <?php echo $registrations->aboutme ?>
                                        </textarea>

                                    </div>
                                    <br/>
                                    
                                    <input type="submit" class="btn btn-success" tabindex="15" name="submit" value="Update" />  
                                    <br/>
                                    <hr/>
                                    

                                </form>

                            </div>
                        </div>
    </section>
			
                <footer>
                    <p>
                        <b>Copyright &copy; Conception | BITM | All Rights Reserved</b>
                    </p>
                </footer>
            </div>

    </body>
</html>
