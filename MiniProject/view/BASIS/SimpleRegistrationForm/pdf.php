<?php

include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniProject' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\SimpleRegistrationForm\Registration;
use App\BITM\Utility\Utility;

$registration = new Registration();

$registrations = $registration->lists();
$trs = "";
?>



<?php

$sino = 0;
foreach ($registrations as $registration):
    $sino++;
    $trs .="<tr>";
    $trs .="<td>" . $sino . "</td>";
    $trs .="<td>" . $registration->fname . "</td>";
    $trs .="<td>" . $registration->lname . "</td>";
    $trs .="<td>" . $registration->birthday . "</td>";
    $trs .="<td>" . $registration->email . "</td>";
    $trs .="<td>" . $registration->phonenumber . "</td>";
    $trs .="<td>" . $registration->city . "</td>";
    $trs .="<td>" . $registration->gender . "</td>";
    $trs .="<td>" . $registration->religious . "</td>";
    $trs .="<td>" . $registration->aboutme . "</td>";
    $trs .="<td>" . $registration->termandcondition . "</td>";
//                    $trs .="<td><img src=../Resources/img/".$book['coverpage']." height='50' width='50' /></td>";
    $trs .="</tr>";
endforeach;
?>


<?php

$html = <<<BITM
<!DOCTYPE html>
<html>
    <head>
        <title>List Of Registred User</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    
    </head>
    
    <body>
        <h1 align="center" style="color:#037CE7;">Registered User Information</h1>
     
        <table border="1">
            <thead>
                <tr style="background:#7DABF6;">
                    <th>Sl.</th>
                    
                    <th>First Name</th>
                    
                    <th>Last Name</th>
        
                    <th>Birthday</th>
        
                    <th>Email Address</th>
        
                    <th>Cell Number</th>
        
                    <th>City</th>
        
                    <th>Gender</th>
        
                    <th>Religion</th>
        
                    <th>About Me</th>
        
                    <th>Terms & Condition</th>

                     
                
                </tr>
            </thead>
            <tbody>
        
              echo $trs;
        
        
        
        </tbody>
        </table>

    </body>
</html>
BITM;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "MiniProject" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;

