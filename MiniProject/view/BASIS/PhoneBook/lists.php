<?php
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'MiniProject' . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\PhoneBook\Phonebook;
use App\BITM\Utility\Utility;

$phonebook = new Phonebook($_POST);
$phonebooks = $phonebook->lists();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Phone Book</title>
        <style>
            
            #utility{
                
                background-color: greenyellow;
            }
            
        </style>
    </head>
    <body>

        <table border="1">
            <label><h2>Phone Book List</h2></label>
            <br/>
            <br/>
            <label><span id="utility" ><?php echo Utility::massage(); ?></span></label>
            <br/>
            <label><span><a href="create.php">Add New</a> | Download as XL/PDF | <a href="trashed.php">All Trashed</a></span></label>
            <tr>             
                <th>SI.</th>
                <th>ID.</th>
                <th>Name</th>
                <th>Phone Number</th>
                <th>Action</th>   
            </tr>
            <?php
            $sino = 1;
            foreach ($phonebooks as $phonebook) {
                ?>
                <tr>
                    <td><?php echo $sino; ?></td>
                    <td><?php echo $phonebook->id; ?></td>
                    <td><?php echo $phonebook->name; ?></td>
                    <td><?php echo $phonebook->phonenumber; ?></td>
                    <td>
                          <a href="show.php?id="><input type="submit" value="View"/></a> 
                        | <a href="edit.php?id="><input type="submit" value="Edit"/></a> 
                        | <a href="trash.php?id="><input type="submit" value="Trash"/></a> 
                        | <a href="delete.php?id="><input type="submit" value="Delete"/></a>
                    </td>     
                </tr>
                <?php
                $sino++;
            }
            ?>

        </table>

    </body>
</html>
