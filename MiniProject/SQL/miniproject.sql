-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2016 at 06:37 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miniproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebooks`
--

CREATE TABLE IF NOT EXISTS `phonebooks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phonebooks`
--

INSERT INTO `phonebooks` (`id`, `name`, `phonenumber`) VALUES
(1, 'oliur', '017655'),
(2, 'Oliur', '01738745183'),
(6, 'ghgfdf', '65445667578'),
(7, 'fjkly', '5555555'),
(8, 'fgm', '01738745183');

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE IF NOT EXISTS `registrations` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `cemail` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `religious` varchar(255) NOT NULL,
  `aboutme` varchar(255) NOT NULL,
  `termandcondition` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`id`, `fname`, `lname`, `birthday`, `email`, `cemail`, `phonenumber`, `city`, `gender`, `religious`, `aboutme`, `termandcondition`, `deleted_at`) VALUES
(54, 'Swajib', 'Hassan', '1981-02-01', 'swajib.engg@gmail.com', 'swajib.engg@gmail.com', '0187887860', 'Dhaka', 'Male', 'Islam', '         sdsdds                               ', 'agree', NULL),
(55, 'Wrehan', 'Hassan', '1081-05-07', 'swajib.engg@gmail.com', 'swajib.engg@gmail.com', '0187887860', 'Chitagong', 'Female', 'Islam', '         sdswd                               ', 'agree', NULL),
(56, 'Apurbo', 'Hassan', '1988-10-11', 'swajib.cse@gmail.com', 'swajib.cse@gmail.com', '01672461905', 'Dhaka', 'Male', 'Islam', '     sdsdsf                                   ', 'agree', NULL),
(57, 'Kumer ', 'Sanu', '1988-10-11', 'swajib.engg@gmail.com', 'swajib.engg@gmail.com', '01672461905', 'Dhaka', 'Male', 'Hinduism', 'Singer                             ', 'agree', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE IF NOT EXISTS `tbl_login` (
  `login_id` int(6) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`login_id`, `email_address`, `password`) VALUES
(1, 'swajib.engg@gmail.com', '107386');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonebooks`
--
ALTER TABLE `phonebooks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registrations`
--
ALTER TABLE `registrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`login_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonebooks`
--
ALTER TABLE `phonebooks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `registrations`
--
ALTER TABLE `registrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `login_id` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
