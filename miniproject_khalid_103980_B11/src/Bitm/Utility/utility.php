<?php
namespace Phone\Bitm\Utility;
//include_once '../vendor/autoload.php';

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."miniproject_khalid_103980_B11".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

session_start();

class utility {
    public $_message = "";
    static public function d($pre=FALSE){
        echo "<pre>";
            var_dump($pre);
            echo "</pre>";
    }
    static public function dd($pre=FALSE){
        self::d($pre);
        die();
    }
    static public function redirect($url=""){
        header("Location: ".$url);
    }
    
    static public function message($message=NULL){
        if(is_null($message)){
            $_message = self::getmessage();
            return $_message;
        }  else {
            self::setmessage($message);
        }
    }
    
    static private function getmessage(){
        $_message = $_SESSION['message'];
        $_SESSION['message']="";
        return $_message;
    }
    static private function setmessage($message){
        $_SESSION['message'] = $message;
    }
}