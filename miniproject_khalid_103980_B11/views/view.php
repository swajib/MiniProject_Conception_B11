<?php

include_once '../vendor/autoload.php';

use Phone\Bitm\Allclass\PhoneBook;
use Phone\Bitm\Utility\utility;

$myphonlist = new PhoneBook();
//utility::dd($_REQUEST['id']);
$obj = $myphonlist->show($_REQUEST['id']);
//utility::dd($obj);
//die();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Personal view</title>
        <link href="../resource/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main>
          <div class="container">
                <div class="header clearfix">
                  <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" ><a href="../index.php">Home</a></li>
                      <li><a href="phonelist.php" >Phone List</a></li>
                      <li  role="presentation" class="active" ><a href="#" >Personal View</a></li>
                      <li role="presentation"><a href="#">About</a></li>
                      <li role="presentation"><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                  <h3 class="text-muted">Mini Project</h3>
                </div>
               <div>
                        <button class="btn btn-warning pull-right"><a style="color:#fff;" href="logout.php">Logout</a></button>
                    </div>
                    <br>
                <div class="row marketing">
                    <div class="col-lg-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                              Personal view
                            </div>
                            <div class="panel-footer">
                                   <table class="table">
                                    <tr>
                                        <td>Name</td>
                                        <td><?php echo $obj['name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Father Name</td>
                                        <td><?php echo $obj['f_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mother Name</td>
                                        <td><?php echo $obj['m_name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Birth Day</td>
                                        <td><?php echo $obj['b_day']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td><?php echo $obj['gender']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone Number</td>
                                        <td><?php echo $obj['p_num']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $obj['email']; ?></td>
                                    </tr>
                                    
                                </table>
                            </div>
                          </div>
                    </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        
                        <?php echo "<img style='height:200px;' class='img-thumbnail' src='".$obj['img_path']."'>";?>
                        <p class="help-block">Profile Picture</p>
                    </div>
                </div>
            </div>

                <footer class="footer">
                  <p>Conception &copy; 2016 </p>
                </footer>
            </div> <!-- /container -->     
                         
                         
        </main>

        <script src="../resource/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/npm.js" type="text/javascript"></script>  
    </body>
</html>
