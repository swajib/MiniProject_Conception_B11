<?php
include_once '../vendor/autoload.php';
use Phone\Bitm\Allclass\PhoneBook;
use Phone\Bitm\Utility\utility;

$myphonlist = new PhoneBook();

$filter = array();
$filtername = isset($_POST['filter']) ? $_POST['filter']:"";
//utility::dd($_POST['filter']);

if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
//    utility::dd($_POST);
    $filter = $_POST;
}
$mylist = $myphonlist->index($filter);

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */



$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Khalid")
							 ->setLastModifiedBy("Khalid")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Serial No')
            ->setCellValue('B1', 'Name')
            ->setCellValue('C1', 'Phone')
            ->setCellValue('D1', 'Email');

$sl = 1;
foreach ($mylist as $pbook) {
$sl++;
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$sl, $sl)
            ->setCellValue('B'.$sl, $pbook['name'])
            ->setCellValue('C'.$sl, $pbook['p_num'])
            ->setCellValue('D'.$sl, $pbook['email']);
}
// Miscellaneous glyphs, UTF-8
//$objPHPExcel->setActiveSheetIndex(0)
//            ->setCellValue('A4', 'Miscellaneous glyphs')
//            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Phone List');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
