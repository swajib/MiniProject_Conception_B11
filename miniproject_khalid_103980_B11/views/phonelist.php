<?php
include_once '../vendor/autoload.php';

use Phone\Bitm\Allclass\PhoneBook;
use Phone\Bitm\Utility\utility;

$myphonlist = new PhoneBook();



$filter = array();
$filtername = isset($_POST['filter']) ? $_POST['filter']:"";
//utility::dd($_POST['filter']);

if(strtoupper($_SERVER['REQUEST_METHOD'])=='POST'){
//    utility::dd($_POST);
    $filter = $_POST;
}
$mylist = $myphonlist->index($filter);

?>

<!DOCTYPE html>

<html>
       <head>
            <meta charset="UTF-8">
            <title>Phone Book List</title>
            <link href="../resource/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/css/style.css" rel="stylesheet" type="text/css"/>
       </head>
       <body>
            <main id="maindiv">
            <div class="container">
                <div class="header clearfix">
                  <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" ><a href="../index.php">Home</a></li>
                        <li role="presentation" class="active"><a href="#">Phone List</a></li>
                      <li role="presentation"><a href="#">About</a></li>
                      <li role="presentation"><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                  <h3 class="text-muted">Mini Project - Registration</h3>
                </div>
                    <h1 class="text-center" style="margin-bottom: 35px;"> <ins>Registration List </ins></h1>
                    <div style="min-height: 30px;">
                        <p id="massage"><?php  echo utility::message(); ?></p>
                  </div>
                    <div>
                        <button class="btn btn-warning pull-right"><a style="color:#fff;" href="logout.php">Logout</a></button>
                    </div>
                    <br>
                    <hr/>
                  <div class="alert alert-success pull-right" role="alert">
                      <a href="trashed.php" class="btn alert-link">
                      <span class="glyphicon glyphicon-briefcase" > TrashList</span></a>
                      <a href="mypdf.php" class="btn alert-link">
                      <span class="glyphicon glyphicon-save" > Pdf</span></a>
                      <a href="excel.php" class="btn alert-link">
                      <span class="glyphicon glyphicon-cloud-download" > Excel</span></a>
                  </div>
                  <div class="alert alert-success pull-left" role="alert">
                      <form action="#" method="POST">
                            <div class="form-group">
                              <label for="go">Filter By Name</label>
                              <input type="text" name="filter" class="form-control" id="go" value="<?php echo $filtername ;?>">
                            </div>
                            <input type="submit" value=" Go">
                        </form>
                  </div>
                  
                  <form action="trashall.php" method="POST">
                      <div class="col-md-12">
                          <button class="btn btn-default trash" type="submit">Trash All</button>
                          
                          <br/><br/>
                      </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" name="markall" id="markall">
                            </th>
                            <th>Serial</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sl = 0;
                            foreach ($mylist as $pbook) {
                            $sl++;
                        ?>
                        <tr>
                            <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $pbook['id'] ;?>"></td>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $pbook['name']; ?></td>
                            <td><?php echo $pbook['p_num']; ?></td>
                            <td><?php echo $pbook['email']; ?></td>
                            <td>
                                <a class="btn btn-primary" href="view.php?id=<?php echo $pbook['id']; ?>">View</a>
                                <a class="btn btn-success" href="edit.php?id=<?php echo $pbook['id']; ?>">Edit</a>
                            </td>
                            <td><a class="btn btn-warning trash" href="trash.php?id=<?php echo $pbook['id']; ?>">Trash</a></td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </from>
            <footer class="footer">
                <p>Conception &copy; 2016 </p>
            </footer>
        </div>
     </main>
              
        <script src="../resource/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/npm.js" type="text/javascript"></script>  
        
        <script>
            $(document).ready( function(){
              $('.trash').bind('click', function(e){
                var deleteItem = confirm('Are you sure to trash?');
                if(!deleteItem){
//                    return false;
                   e.preventDefault();
                }
            });
            $('#massage').hide(7000); 
            
            $('#markall').bind('click', function(){
                if($('#markall').is(':checked')){
                    $('.mark').each(function(){
                       this.checked = true; 
                    });
                }else{
                    $('.mark').each(function(){
                       this.checked =false; 
                    });
                }
                
            });
            
           
              $('#deleteAll').bind('click', function(e){
                var deleteItem = confirm('Are you sure to delete all items?');
                if(deleteItem){
                    document.forms[0].action='deleteall.php';
                    document.forms[0].submit();
                }
            });
                
            });
         </script>
       </body>
</html>
