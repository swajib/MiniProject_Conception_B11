<?php
include_once '../vendor/autoload.php';

use Phone\Bitm\Allclass\PhoneBook;
use Phone\Bitm\Utility\utility;

$myphonlist = new PhoneBook();

$mylist = $myphonlist->trashed();
//utility::dd($mylist);


?>

<!DOCTYPE html>

<html>
       <head>
            <meta charset="UTF-8">
            <title>Phone Book Trash List</title>
            <link href="../resource/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="../resource/css/style.css" rel="stylesheet" type="text/css"/>
       </head>
       <body>
            <main id="maindiv">
            <div class="container">
                <div class="header clearfix">
                  <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" ><a href="../index.php">Home</a></li>
                        <li role="presentation"><a href="phonelist.php">Phone List</a></li>
                      <li role="presentation"><a href="#">About</a></li>
                      <li role="presentation"><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                  <h3 class="text-muted">Mini Project</h3>
                </div>
                    <h1 class="text-center" style="margin-bottom: 35px;"> <ins>Phone Book Trash List </ins></h1>
                    <div style="min-height: 30px;">
                        <p id="massage"><?php echo utility::message(); ?></p>
                  </div>
                     <div>
                        <button class="btn btn-warning pull-right"><a style="color:#fff;" href="logout.php">Logout</a></button>
                    </div>
                    <br>
                    <hr/>
                  <div class="alert alert-success pull-right" role="alert">
                      <a href="mypdf.php" class="alert-link">
                      <span class="glyphicon glyphicon-save" >Pdf</span></a>
                  </div>
                  <form action="recoverall.php" method="POST">
                      <div>
                          <button class="btn btn-default" type="submit">RecoverAll</button>
                          <button class="btn btn-default" type="button" id="deleteAll">DeleteAll</button><br/><br/>
                      </div>
              <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" name="markall" id="markall">
                            </th>
                            <th>Serial</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Action</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(count($mylist)>0){
                            $sl = 0;
                            foreach ($mylist as $pbook) {
                            $sl++;
                        ?>
                        <tr>
                            <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $pbook['id'] ;?>"></td>
                            <td><?php echo $sl; ?></td>
                            <td><?php echo $pbook['name']; ?></td>
                            <td><?php echo $pbook['p_num']; ?></td>
                            <td><?php echo $pbook['email']; ?></td>
                            <td><a class="btn btn-primary" href="recover.php?id=<?php echo $pbook['id']; ?>">Recover</a></td>
                            <td><a class="btn btn-danger" href="delete.php?id=<?php echo $pbook['id']; ?>">Delete</a></td>
                        <?php
                        }
                        }
                        else{
                        ?>
                            <td class="text-center" colspan="7"> No data is found</td>
                        <?php 
                        }
                        ?>
                        </tr>
                    </tbody>
              </table>
            </form>
            <footer class="footer">
                <p>Conception &copy; 2016 </p>
            </footer>
        </div>
     </main>
              
        <script src="../resource/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/npm.js" type="text/javascript"></script>  
        
        <script>
            $(document).ready( function(){
              $('.delete').bind('click', function(e){
                var deleteItem = confirm('Are you sure to delete?');
                if(!deleteItem){
//                    return false;
                   e.preventDefault();
                }
            });
            $('#massage').hide(7000); 
            
            $('#markall').bind('click', function(){
                if($('#markall').is(':checked')){
                    $('.mark').each(function(){
                       this.checked = true; 
                    });
                }else{
                    $('.mark').each(function(){
                       this.checked =false; 
                    });
                }
                
            });
            
           
              $('#deleteAll').bind('click', function(e){
                var deleteItem = confirm('Are you sure to delete all items?');
                if(deleteItem){
                    document.forms[0].action='deleteall.php';
                    document.forms[0].submit();
                }
            });
                
            });
         </script>
       </body>
</html>
