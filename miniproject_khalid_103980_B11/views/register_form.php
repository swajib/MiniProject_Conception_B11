<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registation Page</title>
        <link href="../resource/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../resource/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main id="maindiv">
            <div class="container">
                <div class="header clearfix">
                  <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" ><a href="../index.php">Home</a></li>
                        <li role="presentation" class="active"><a href="#">Registation</a></li>
                      <li role="presentation"><a href="#">About</a></li>
                      <li role="presentation"><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                  <h3 class="text-muted">Mini Project</h3>
                </div>
                <div class="row marketing">
                    <h1 class="text-center" style="margin-bottom: 35px;"> <ins>Register  Here </ins></h1>
                    <div style="min-height: 30px;">
                      <p></p>
                  </div><hr/>
                    <div class="col-lg-6">
                        <form action="store.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                          <label for="name">Your Name</label>
                          <input type="text" tabindex="1" class="form-control" id="name" placeholder="Enter Name" name="name" autofocus="autofocus" required>
                        </div>
                        <div class="form-group">
                          <label for="f_name">Father's Name</label>
                          <input type="text" class="form-control" id="f_name" placeholder="Father Name" tabindex="2" name="f_name">
                        </div>
                        <div class="form-group">
                          <label for="m_name">Mother's Name</label>
                          <input type="text" class="form-control" id="m_name" placeholder="Mother Name" name="m_name" tabindex="3">
                        </div>
                        <div class="form-group">
                          <label for="b_day">Birth Day</label>
                          <input type="date" class="form-control" id="b_day" placeholder="YY-MM-DD" tabindex="4" name="b_day">
                        </div>
                        <div class="form-group">
                          <label for="p-num">Phone Number</label>
                          <input type="text" class="form-control" id="p-num" placeholder="Phone Number" name="p_num" tabindex="5" required pattern="[0-9]{7,11}" >
                        </div>
                        <div class="form-group">
                          <label for="Email1">Gender</label>
                          <div class="radio">
                            <label>
                                <input type="radio" name="gender" id="optionsRadios1" value="Male" checked tabindex="6">
                              Male
                            </label>
                          </div>
                          <div class="radio">
                            <label>
                                <input type="radio" name="gender" id="optionsRadios2" value="Female" tabindex="7">
                              Female
                            </label>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="dist">City</label>
                            <select name="city" id="dist" class="form-control" tabindex="8">
                                <option value=" " >Division</option>
                                <option value="Barisal">Barisal</option>
                                <option value="Chittagoan">Chittagoan</option>
                                <option value="Dhaka">Dhaka</option>
                                <option value="Khulna">Khulna</option>
                                <option value="Mymensingh">Mymensingh</option>
                                <option value="Rajsahi">Rajsahi</option>
                                <option value="Rangpur">Rangpur</option>
                                <option value="Sylhet">Sylhet</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="add">Address</label>
                          <textarea name="address" class="form-control" rows="4" id="add" tabindex="9"></textarea>
                        </div>
                    </div>

                <div class="col-lg-6">
                    <label for="Email1">Profile Picture</label>
                    <img class="img-thumbnail" src="../resource/image/photo-1.jpg" alt=""/>
                        <div class="form-group">
                            <label for="File">File input</label>
                            <input type="file" id="File" name="profile_img" tabindex="10">
                            <p class="help-block">Upload your Image</p>
                        </div>
                    <div class="form-group">
                          <label for="Email1">Email address</label>
                          <input name="email" type="email" class="form-control" id="Email1" placeholder="Email" tabindex="11" required  >
                        </div>
                        <div class="form-group">
                          <label for="Password1">Password</label>
                          <input name="password" type="password" class="form-control" id="Password1" placeholder="Password" tabindex="12" required pattern=".{6,}" title="Six or more characters">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="condition" value="Agree" tabindex="13" required> I agree the <a href="#">Terms & Policies</a>
                            </label>
                          </div>
                        <button type="submit" class="btn btn-success" tabindex="14">Submit</button>
                        <button type="reset" class="btn btn-primary" tabindex="15">Reset</button>
                  </form>
                </div>
            </div>

                <footer class="footer">
                  <p>Conception &copy; 2016 </p>
                </footer>
            </div> <!-- /container -->
        </main>
        <script src="../resource/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../resource/lib/bootstrap/js/npm.js" type="text/javascript"></script>
    </body>
</html>
