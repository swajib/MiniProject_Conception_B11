-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 26, 2016 at 11:43 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mini_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `f_name` varchar(55) NOT NULL,
  `m_name` varchar(255) NOT NULL,
  `b_day` date NOT NULL,
  `p_num` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `conditions` varchar(50) NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `delete_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `f_name`, `m_name`, `b_day`, `p_num`, `gender`, `city`, `address`, `email`, `password`, `conditions`, `img_path`, `delete_at`) VALUES
(7, 'Khadiza', 'khalid', 'Farhana', '2010-10-16', '01751245084', 'Male', 'Dhaka', 'House#243, Mirpur, Dhaka                          ', 'khadiza@gmail.com', '28564789', '', '../resource/upload/', '1453795072'),
(8, 'Khadiza', 'khalid', 'Farhana', '2010-10-16', '01751245084', 'Male', 'Dhaka', ' House#243, Mirpur, Dhaka                                                    ', 'khadiza@gmail.com', '28564789', '', '../resource/upload/Pasport Pic.jpg', ''),
(9, 'Khadiza', 'khalid', 'Farhana', '2010-10-16', '01751245084', 'Male', 'Dhaka', '                               House#243, Mirpur, Dhaka                                                                              ', 'khadiza@gmail.com', '28564789', '', '../resource/upload/Image0013-compressor.jpg', '1453800092'),
(10, 'New Person', 'New Person', 'New Person', '2000-01-01', '0127226523', 'Male', 'Mymensingh', 'This is rest', 'newone@gmail.com', '258963', 'Agree', '../resource/upload/DSCN1018-compressor.jpg', '1453800502'),
(11, 'New Person', 'New Person', 'New Person', '2000-01-01', '0127226523', 'Male', 'Mymensingh', 'This is rest                          ', 'newone@gmail.com', '258963', '', '../resource/upload/DSCN0962.JPG', '1453840470'),
(12, 'Khadiza', 'khalid', 'Farhana', '2010-10-16', '01751245084', 'Male', 'Dhaka', ' House#243, Mirpur, Dhaka                                                    ', 'khadiza@gmail.com', '28564789', '', '../resource/upload/DSCN0962.JPG', NULL),
(13, 'Khadiza2', 'New father', 'Farhana', '2010-10-16', '01751245084', 'Female', 'Rajsahi', 'House#243, Mirpur, Dhaka                                                                              ', 'khadiza@gmail.com', '28564789', '', '../resource/upload/', '1453795745'),
(14, 'Swajib Hassan', 'Azad', 'Swapna', '1988-11-10', '01672461905', 'Male', 'Dhaka', 'Dhaka', 'swajib.engg@gmail.com', '123456', 'Agree', '../resource/upload/100_1381.jpg', NULL),
(15, 'Md. Al-Kawser', 'Samsuddin Ahmed', 'Rahima Begum', '1992-01-08', '01920314909', 'Male', 'Dhaka', '                                                            19/ka, East Madertek, Singapore Road, Bashaboo, Dhaka-1214                                                    ', 'alkawser@yahoo.com', '222222', 'Agree', '../resource/upload/', '1453800516'),
(16, 'Rokon', 'Shekader', 'Reba', '1992-12-31', '01737322378', 'Male', 'Rangpur', '                                                                                          29/C,Rajabazer,Dhaka                                                                              ', 'rokonodulla@gmail.com', '01737322378', 'Agree', '../resource/upload/20150615_205823.jpg', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
