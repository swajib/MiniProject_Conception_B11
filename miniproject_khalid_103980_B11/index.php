    <?php
//    include_once './vendor/autoload.php';
    
    include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."miniproject_khalid_103980_B11".DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."Bitm".DIRECTORY_SEPARATOR."Utility".DIRECTORY_SEPARATOR."utility.php");


    use Phone\Bitm\Utility\utility;

    ?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="resource/lib/bootstrap/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
        <link href="resource/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="resource/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="resource/css/style.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <main id="maindiv">
            <div class="container">
                <div class="header clearfix">
                  <nav>
                    <ul class="nav nav-pills pull-right">
                      <li role="presentation" class="active"><a href="#">Home</a></li>
                      <li><a href="./views/phonelist.php" >Registration List</a></li>
                      <li role="presentation"><a href="#">About US</a></li>
                      <li role="presentation"><a href="#">Contact US</a></li>
                    </ul>
                  </nav>
                  <h3 class="text-muted">Mini Project - Registration</h3>
                </div>
                <div style="min-height: 30px;">
                    <p id="massage" class="text-center"><?php 
                        if($_SESSION!=NULL){
                            echo utility::message();
                        }
                     ?></p>
                  </div><hr/>
                <div class="row marketing">
                    <div class="col-lg-6">
                        <div class="jumbotron">
                            <h2>Registration Form</h2>
                            <p class="lead">Register Here</p>
                            <p><a class="btn btn-lg btn-success" href="./views/register_form.php" role="button">Sign Up</a></p>
                        </div>
                    </div>
                    
                <div class="col-lg-6">
                    <form action="views/checklogin.php" method="POST">
                        <div class="form-group">
                          <label for="Email1">Email address</label>
                          <input name="email" type="email" class="form-control" id="Email1" placeholder="Email Address">
                        </div>
                        <div class="form-group">
                          <label for="Password1">Password</label>
                          <input name="password" type="password" class="form-control" id="Password1" placeholder="Password Here">
                        </div>

                        <div class="checkbox">
                          <label>
                            <input type="checkbox"> Remember Me
                          </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>

                <footer class="footer">
                  <p>Conception &copy; 2016 </p>
                </footer>
            </div> <!-- /container -->
        </main>
        <script src="resource/js/jquery-2.1.3.min.js" type="text/javascript"></script>
        <script src="resource/lib/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="resource/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="resource/lib/bootstrap/js/npm.js" type="text/javascript"></script>
        <script>
            $('#massage').hide(7000); 
        </script>
    </body>
</html>
